<?php

namespace VentureOak\DRubyBundle\Controller;

use Doctrine\Bundle\DoctrineBundle\Mapping\DisconnectedMetadataFactory;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Gedmo\Exception\RuntimeException;
use Metadata\ClassMetadata;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\Annotation\Route;
use VentureOak\DRubyBundle\Library\Exceptions\SingularException;
use VentureOak\DRubyBundle\Library\SingularValidation;
use VentureOak\DRubyBundle\Library\Validation\IsInt;

class DefaultController extends Controller
{
    /**
     * @Route(path="/", name="ruby_index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $accessor = PropertyAccess::createPropertyAccessor();
        //get all entities:
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();

        $dataArray = [];
        /** @var ClassMetadata $entity */
        foreach ( $entities as $sEntity) {
            if (!in_array($sEntity, ['FOS\UserBundle\Model\User', 'FOS\UserBundle\Model\Group'])) {
                $classMetaData = $em->getClassMetadata($sEntity);
                $dataArray[$sEntity]['attributes'] = $classMetaData->getFieldNames();
                $dataArray[$sEntity]['associations'] =$classMetaData->getAssociationMappings();

            }
        }

        return $this->render('DRubyBundle:Default:index.html.twig', ['data' => $dataArray]);
    }

    /**
     * @Route(path="/v2", name="ruby_v2")
     */
    public function index2Action()
    {
        $excludeEntities = ['FOS\UserBundle\Model\User', 'FOS\UserBundle\Model\Group'];

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();
        $arr = [];

        foreach ($entities as $ent) {
            if (!in_array($ent, $excludeEntities)) {
                $entityMetaData = $em->getClassMetadata($ent);

                $arr[$ent]['attributes'] = $entityMetaData->getFieldNames();
                //get association mapping
                foreach ( $entityMetaData->getAssociationMappings() as $assoc) {
                    $arr[$ent]['associations'][$assoc['fieldName']] = [
                      'targetEntity' => $assoc['targetEntity'],
                        'mappedBy' => $assoc['mappedBy'],
                        'inversedBy' => $assoc['inversedBy'],
                        'type' => $assoc['type']
                    ];
                }
//                $arr[$ent]['associations'] = $entityMetaData->getAssociationMappings();
                $arr[$ent]['associationNames'] = $entityMetaData->getAssociationNames();
            }
        }


        return $this->render('DRubyBundle:Default:v2.html.twig', ['data' => $arr]);
    }

    /**
     * @Route(path="/process/{jsonString}", name="ruby_process")
     */
    public function processAction($jsonString = null)
    {
        $rubySvc = $this->get('ruby.job_service');
        $rubySvc->setJobStructure($jsonString);

        $result = $rubySvc->runJob();

        return $this->render('DRubyBundle:Default:process.html.twig', ['jobStructure' => $jsonString, 'result' => $result]);


//        $em = $this->get('doctrine.orm.default_entity_manager');
//        $accessor = PropertyAccess::createPropertyAccessor();
//
//        $data = json_decode($jsonString);
//        $validatedEntities = 0;
//        $totalEntities =0;
//        foreach ($data as $entity => $stuff) {
//
//            $repo = $em->getRepository($entity);
//            $rows = $repo->findAll();
//            $totalEntities = count($rows);
//            dump("Applying validations for entity ".$entity. ":: ".$totalEntities);
//            foreach ($rows as $row) {
//                $attributes = $stuff->attributes;
//                foreach ($attributes as $attr => $rules) {
//                    try {
//                        $value = $accessor->getValue($row, $attr);
//                        $validation = $rules->condition;
//                        $expected = $rules->value;
////                        $validator = new SingularValidation($attr, $value);
//                        $validator = new IsInt($attr, $value);
//                        $validator->apply();
//                        $validatedEntities++;
////                    $validator->min(2);
//
//                    } catch (SingularException $ex) {
//                        dump($ex->getCode() . '=>'. $ex->getMessage(), $row);
//                    } catch (RuntimeException $ex) {
//                        dump($ex->getCode(), $ex->getMessage());
//                    } catch (\Exception $ex) {
//                        dump('Unknown Exception', $ex->getMessage());
//                    }
//                }
//            }
//            dump("Validated Entities: ".$validatedEntities. '/'.$totalEntities);
//            $validatedEntities =0;
//        }
//
//
//        die;
//
//
//        $flag = true;
//        foreach ($data as $entity => $stuff) {
//
//            $repo = $em->getRepository($entity);
//            if ($flag) {
//                $qb = $repo->createQueryBuilder($em->getClassMetadata($entity)->getName());
//
//                //set where statement
//                //attributes
//                foreach ($stuff->attributes as $field => $rules) {
//                    $qb->andWhere(sprintf("%s.%s %s %s", $em->getClassMetadata($entity)->getName(),$field, $rules->condition, $rules->value));
//                }
//                foreach($stuff->associations as $class => $rules) {
//                    $f1 = false;
//                    foreach($rules as $rule) {
//                        if (!$f1){
//                            $qb->innerJoin($entity.'.'.$rule->associationName, $rule->associationName );
//                            $f1 = true;
//                        }
//                        $qb->andWhere(sprintf("%s.%s %s %s", $rule->associationName, $rule->field, $rule->condition, $rule->value ));
////                        var_dump($rule); die;
//                    }
//
//                }
//            } else {
//                //try to join entity
//                $em->getClassMetadata($entity)->getAssociationMappings();
//            }
//
//
//        }
//        echo($qb->getQuery()->getSQL());
//
//        die;
    }
}
