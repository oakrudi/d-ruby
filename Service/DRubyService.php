<?php

/**
 * User: rudi <rrocha@ventureoak.com>
 * Date: 09-12-2015
 * Project bshow
 */

namespace VentureOak\DRubyBundle\Service;

use Doctrine\ORM\EntityManager;
use RuntimeException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use VentureOak\DRubyBundle\Library\Exceptions\SingularException;
use VentureOak\DRubyBundle\Library\RubyValidation;
use VentureOak\DRubyBundle\Library\RubyValidationFactory;

class DRubyService
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /** @var  PropertyAccess */
    private $accessor;

    /**
     * @var string
     */
    private $jobStructure;

    /**
     * @var array
     */
    private $validationErrors;

    /**
     * @var int
     */
    private $validateSuccessEntities;

    /**
     * @var int
     */
    private $totalFetchedEntities;

    /**
     * DRubyService constructor.
     */
    public function __construct($em)
    {
        $this->entityManager = $em;
        $this->accessor = PropertyAccess::createPropertyAccessor();
        $this->validateSuccessEntities = 0;
    }


    /**
     * @return array
     */
    public function runJob()
    {
        $response = [];

        foreach ($this->getJobStructure() as $sEntity => $aGroup) {
            $repo = $this->getEntityManager()->getRepository($sEntity);

            /* TODO: make this fetch more flexible considering user input filters  */
            $dbRows = $repo->findAll();
            $this->totalFetchedEntities = count($dbRows);

            foreach ($dbRows as $entity)
            {
                $entityAttributes = $aGroup->attributes;

                //validate attributes
                foreach ($entityAttributes as $attr => $rules) { //TODO: Make rules possible to have multiple validations to each attribute
                    try {

                        $value = $this->getAccessor()->getValue($entity, $attr);
                        $validationRule = $rules->condition;
                        $expected = $rules->value;

                        /** @var RubyValidation $validator */
                        $validator = RubyValidationFactory::getSingularValidator(ucfirst($validationRule), $attr, $value, $expected);
                        $validator->apply();

                        $this->addValidatedSuccessEntity();

                    } catch (SingularException $ex) {
                        $this->addValidationError($entity, $ex->getMessage());
                    } catch (RuntimeException $ex) {
                        $this->addValidationError($entity, $ex->getMessage());
                    } catch (\Exception $ex) {
                        dump('Critical Exception', $ex->getMessage()); die; //TODO: Handle this die
                    }
                }

            }

            $response ['counters'] = [
                $sEntity => [
                    'totalEntities' => $this->getTotalFetchedEntities(),
                    'validatedEntities' => $this->getValidateSuccessEntities(),
                ]
            ];

            $this->validateSuccessEntities =0;
        }

        $response ['errors'] = $this->getValidationErrors();

        return $response;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getJobStructure()
    {
        return json_decode($this->jobStructure);
    }

    /**
     * @param string $jobStructure
     */
    public function setJobStructure($jobStructure)
    {
        $this->jobStructure = $jobStructure;
    }

    /**
     * @return PropertyAccessor
     */
    public function getAccessor()
    {
        return $this->accessor;
    }

    /**
     * @return array
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }

    /**
     * @param array $validationErrors
     */
    public function setValidationErrors($validationErrors)
    {
        $this->validationErrors = $validationErrors;
    }

    /**
     * @param $entity
     * @param $error
     */
    public function addValidationError($entity, $error)
    {
        $this->validationErrors[] = [$entity, $error];
    }

    /**
     * @return int
     */
    public function getValidateSuccessEntities()
    {
        return $this->validateSuccessEntities;
    }

    /**
     * @return int
     */
    public function addValidatedSuccessEntity()
    {
        return $this->validateSuccessEntities++;
    }

    /**
     * @return int
     */
    public function getTotalFetchedEntities()
    {
        return $this->totalFetchedEntities;
    }



}