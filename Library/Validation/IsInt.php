<?php
/**
 * User: rudi <rrocha@ventureoak.com>
 * Date: 07-12-2015
 * Project bshow
 */

namespace VentureOak\DRubyBundle\Library\Validation;

use VentureOak\DRubyBundle\Library\Exceptions\SingularException;
use VentureOak\DRubyBundle\Library\RubyValidation;
use VentureOak\DRubyBundle\Library\SingularValidation;

class IsInt extends SingularValidation implements RubyValidation
{

    /**
     * Executes the validation and returns the value
     * @return mixed
     * @throws SingularException
     */
    public function apply()
    {
        $flag = $this->execute();

        if (!$flag && $this->isThrowsException()) {
            throw new SingularException(
                $this->getValidationCode(),
                sprintf("[%s] field failed validation [%s] with value [%s]",
                    $this->getFieldName(),
                    $this->getValidationCode(),
                    $this->getValue()
                )
            );
        }

        return $flag;
    }

    /**
     * Returns validation string
     * @return string
     */
    public function getValidationCode()
    {
        return 'isInt';
    }

    /**
     * @return bool
     */
    private function execute()
    {
        if (is_int($this->getValue())) {
            return true;
        }
        return false;
    }
}