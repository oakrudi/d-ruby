<?php
/**
 * User: rudi <rrocha@ventureoak.com>
 * Date: 07-12-2015
 * Project bshow
 */

namespace VentureOak\DRubyBundle\Library;


interface RubyValidation
{
    /**
     * Returns validation string
     * @return string
     */
    public function getValidationCode();

    /**
     * Executes the validation and returns the value
     * @return mixed
     */
    public function apply();
}