<?php
/**
 * User: rudi <rrocha@ventureoak.com>
 * Date: 09-12-2015
 * Project bshow
 */

namespace VentureOak\DRubyBundle\Library;


class RubyValidationFactory
{

    /**
     * @param $sStrategy
     * @param $field
     * @param $value
     * @param null $expectedValue
     * @return SingularValidation
     */
    static function getSingularValidator($sStrategy, $field, $value, $expectedValue = null)
    {

        $className = sprintf('VentureOak\DRubyBundle\Library\Validation\%s', $sStrategy);

        if (!class_exists($className)) {
            throw new \RuntimeException(sprintf('There are no Validation [%s]', $sStrategy));
        }

        /** @var SingularValidation $obj */
        $obj = new $className($field, $value);
        $obj->setExpectedValue($expectedValue);

        return $obj;
    }
}