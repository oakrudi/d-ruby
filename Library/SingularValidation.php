<?php

/**
 * User: rudi <rrocha@ventureoak.com>
 * Date: 07-12-2015
 * Project bshow
 */

namespace VentureOak\DRubyBundle\Library;

class SingularValidation
{
    /**
     * @var bool
     */
    private $throwsException;

    /**
     * @var mixed
     */
    private $expectedValue;

    /**
     * SingularValidation constructor.
     * @param $fieldName
     * @param $value
     * @param bool $throwException
     */
    public function __construct($fieldName, $value, $throwException = true)
    {
        $this->fieldName = $fieldName;
        $this->value = $value;
        $this->throwsException = $throwException;
    }

    /**
     * @return mixed
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set Value
     * @param $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return boolean
     */
    public function isThrowsException()
    {
        return $this->throwsException;
    }

    /**
     * @return mixed
     */
    public function getExpectedValue()
    {
        return $this->expectedValue;
    }

    /**
     * @param mixed $expectedValue
     */
    public function setExpectedValue($expectedValue)
    {
        $this->expectedValue = $expectedValue;
    }




//{
//
//
//
//    /**
//     * Validate that field has different value from expected
//     * @param $expectedValue
//     * @return bool
//     * @throws SingularException
//     */
//    public function isDifferent($expectedValue){
//        $code = 'isDifferent';
//
//        if ($this->value != $expectedValue) {
//            return true;
//        }
//
//        throw new SingularException(
//            $code,
//            sprintf(
//                "Field Value [%s] fails validation [%s]. Expected [%s], Value [%s]",
//                $this->fieldName,
//                $code,
//                $expectedValue,
//                $this->value
//
//            )
//        );
//    }
//
//    /**
//     * Validate that field has same value from expected
//     * @param $expectedValue
//     * @return bool
//     * @throws SingularException
//     */
//    public function isEquals($expectedValue)
//    {
//        $code = 'isEquals';
//
//        if ($this->value == $expectedValue) {
//            return true;
//        }
//
//        throw new SingularException(
//            $code,
//            sprintf(
//                "Field Value [%s] fails validation [%s]. Expected [%s], Value [%s]",
//                $this->fieldName,
//                $code,
//                $expectedValue,
//                $this->value
//
//            )
//        );
//    }
//
//    public function hasValue($expected)
//    {
//
//    }
}