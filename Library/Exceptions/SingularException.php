<?php
/**
 * User: rudi <rrocha@ventureoak.com>
 * Date: 07-12-2015
 * Project bshow
 */

namespace VentureOak\DRubyBundle\Library\Exceptions;


class SingularException extends RubyException
{

    /**
     * SingularException constructor.
     */
    public function __construct($code, $message)
    {
        $this->code = $code;
        $this->message = $message;
    }
}